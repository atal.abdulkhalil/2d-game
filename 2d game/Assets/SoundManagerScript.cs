using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManagerScript : MonoBehaviour
{
    public static AudioClip bounceSound;
    public static AudioClip failSound;
    static AudioSource audioSource;
    // Start is called before the first frame update
    void Start()
    {
        bounceSound = Resources.Load<AudioClip>("bots");
        failSound = Resources.Load<AudioClip>("fail");
        audioSource = GetComponent<AudioSource>();
    }

    public static void playSound(string sound)
    {
        if (sound == "bounce") audioSource.PlayOneShot(bounceSound);
        if (sound == "fail") audioSource.PlayOneShot(failSound);

    }

}
